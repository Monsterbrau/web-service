<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Student;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/rest", name="rest")
 */

class RestController extends Controller
{

    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @Route("/", methods={"GET"})
     */

    public function index()
    {

        $repo = $this->getDoctrine()->getRepository(Student::class);
        $json = $this->serializer->serialize($repo->findAll(), "json");
        return JsonResponse::fromJsonString($json);

    }

    /**
     * @Route("/",methods={"POST"})
     */

    public function add(Request $request)
    {
        $body = $request->getContent();
        $student = $this->serializer->deserialize($body,Student::class,"json");
    }
}
